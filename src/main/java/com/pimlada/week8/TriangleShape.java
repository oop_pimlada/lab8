package com.pimlada.week8;

public class TriangleShape {
    private String name;
    private double a;
    private double b;
    private double c;

    public TriangleShape(String name, double a, double b, double c) {
        this.name = name;
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double areaTriangle() {
        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));
        System.out.println("A: " + a);
        System.out.println("B: " + b);
        System.out.println("C: " + c);
        System.out.println("Area of Triagle: " + area);
        System.out.println();
        return area;
    }

    public double perimeterTriangle() {
        double perimeter = a + b + c;
        System.out.println("A: " + a);
        System.out.println("B: " + b);
        System.out.println("C: " + c);
        System.out.println("Perimeter of Triangle: " + perimeter);
        System.out.println();
        return perimeter;
    }

    public String getName() {
        return name;
    }
    public double getA() {
        return a;
    }
    public double getB() {
        return b;
    }
    public double getC() {
        return c;
    }
}
