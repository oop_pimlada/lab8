package com.pimlada.week8;

public class CircleShape {
    private String name;
    private double r;

    public CircleShape(String name, double r) {
        this.name = name;
        this.r = r;
    }

    public double areaCircle() {
        double area = 3.14 * r * r;
        System.out.println("Radius: " + r);
        System.out.println("Area of Cicle: " + area);
        System.out.println();
        return area;
    }

    public double perimeterCicle() {
        double perimeter = 2 * 3.14 * r;
        System.out.println("Radius: " + r);
        System.out.println("Perimeter of Circle: " + perimeter);
        System.out.println();
        return perimeter;
    }

    public String getName() {
        return name;
    }
    public double getR() {
        return r;
    }
}
