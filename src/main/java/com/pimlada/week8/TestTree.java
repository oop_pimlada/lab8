package com.pimlada.week8;

public class TestTree {
    private String name;
    private int x;
    private int y;
    public final static int X_MIN = 0;
    public final static int X_MAX = 19;
    public final static int Y_MIN = 0;
    public final static int Y_MAX = 19;

    public TestTree(String name, int x, int y) {
        this.name = name;
        this.x = x;
        this.y = y;
    }

    public void print() {
        System.out.println(name + " x:" + x + " y:" + y);
    }

    public String getName() {
        return name;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
