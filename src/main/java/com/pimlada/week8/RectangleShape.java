package com.pimlada.week8;

public class RectangleShape {
    private String name;
    private double width;
    private double height;

    public RectangleShape(String name, double width, double height) {
        this.name = name;
        this.width = width;
        this.height = height;
    }

    public double areaRectangle() {
        double area = width * height;
        System.out.println("Width: " + width);
        System.out.println("Height: " + height);
        System.out.println("Area of Rectangle: " + area);
        System.out.println();
        return area;
    }

    public double perimeterRectangle() {
        double perimeter = (width + height) * 2;
        System.out.println("Width: " + width);
        System.out.println("Height : " + height);
        System.out.println("Perimeter of Rectangle: " + perimeter);
        System.out.println();
        return perimeter;
    }

    public String getName() {
        return name;
    }
    public double getWidth() {
        return width;
    }
    public double getHeight() {
        return height;
    }
}
